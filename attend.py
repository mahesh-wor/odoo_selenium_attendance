from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import sys


if len(sys.argv) > 2 or len(sys.argv) == 1 :
    print("Error Input : Multiple Option")
    print("""Usuage: python3 attend.py checkin
        python3 attend.py checkout """)
    exit(1)

elif (len(sys.argv) == 2) and (str(sys.argv[1]) == "checkin" or str(sys.argv[1]) == "checkout"):
    print(str(sys.argv[1]).capitalize() )
    option = webdriver.ChromeOptions()
    option = Options()
    # option.add_argument("--headless")
    option.add_argument("--incognito")

    browser = webdriver.Chrome(executable_path="./chromedriver", chrome_options=option)
    url = "https://bisolutions.com.np/web#action=332&menu_id=206"

    browser.get(url)

    username = browser.find_element_by_id("login").send_keys("username")
    password = browser.find_element_by_id("password").send_keys("password")
    browser.find_element_by_xpath("/html/body/div/main/div/form/div[3]/button").click()

    # time.sleep(10)
    browser.implicitly_wait(5)
    state = browser.find_element_by_xpath("/html/body/div[1]/main/div[2]/div/div/div[2]/h3[2]/b")
    if str(state.text) == "check in" and str(sys.argv[1]) == "checkin":
        browser.find_element_by_xpath("/html/body/div[1]/main/div[2]/div/div/div[2]/a").click()
        print("Successsfully Checked In.")
    elif str(state.text) == "check out" and str(sys.argv[1]) == "checkout":
        browser.find_element_by_xpath("/html/body/div[1]/main/div[2]/div/div/div[2]/a").click()
        print("Successsfully Checked Out.")
    elif str(state.text) == "check out" and str(sys.argv[1]) == "checkin":
        print("Already Checked In. Please CheckOut First.")
    elif str(state.text) == "check in" and str(sys.argv[1]) == "checkout":
        print("Already Checked Out. Please CheckIn First.")
    else:
        print("Unknown Error : Contact Admin.")

else:
    print("Error Input : Unrecognized Input")
    print("""Usuage: python3 attend.py checkin
        python3 attend.py checkout """)
    exit(1)






